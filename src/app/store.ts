import { QuizManageAPI } from 'src/services/quizManage';
import { QuizAPI } from 'src/services/quiz';
import { LoginWarranty } from 'src/services/loginWarranty';
import { Warranty } from 'src/services/warranty';
import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import TreeReducer from 'src/services/treeAPI';
import { AuthApi } from 'src/services/auth';
import { TreeAPI } from 'src/services/treeAPI';
import { Facebook } from 'src/services/facebook';

export const store = configureStore({
  reducer: {
    Trees: TreeReducer,
    [Warranty.reducerPath]: Warranty.reducer,
    [LoginWarranty.reducerPath]: LoginWarranty.reducer,
    [AuthApi.reducerPath]: AuthApi.reducer,
    [TreeAPI.reducerPath]: TreeAPI.reducer,
    [Facebook.reducerPath]: Facebook.reducer,
    [QuizAPI.reducerPath]: QuizAPI.reducer,
    [QuizManageAPI.reducerPath]: QuizManageAPI.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      LoginWarranty.middleware,
      Warranty.middleware,
      AuthApi.middleware,
      TreeAPI.middleware,
      Facebook.middleware,
      QuizAPI.middleware,
      QuizManageAPI.middleware,
    ]),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
